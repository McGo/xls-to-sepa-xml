# XLS to SEPA XML

## About

This project contains the laravel installation that is also hosted on xls-to-sepa.mirko-haaser.de. It is intended to help you create your own SEPA XML files from a list of account or payment information entries providing a predefined xls file.

## Disclaimer

Use the provided XML file on your own risk. Since the underlying SEPA repository from https://github.com/AbcAeffchen/Sephpa is tested as well as the XML creationg from importing a xls, I am not repsonsible for anything that might go wrong. To make it short: You use the project and the website on your own risk and are supposed to check all generated files before handing them over to your bank.

