@extends('layout')
@section('content')
    <div class="w-full h-full flex flex-wrap justify-center content-center p-2 sm:p-0">
        <div class="p-4 bg-white border rounded shadow w-full sm:w-2/3 md:w-1/2">
            <div class="border-b p-4">
                <h1 class="font-extrabold text-2xl pb-2">SEPA XLS to XML converter</h1>
                <p>This website helps you convert a XLS file into a SEPA XML file which then could be used as payment
                    information in
                    the banking application of your choice.</p>
                <p class="pt-2">Since using a third party service for converting account and payment information is
                    quite
                    sensitive, there is <a href="/privacy" class="underline">a page describing how this website handles
                        your
                        privacy</a>.</p>
            </div>
            <div class="p-4 flex flex-wrap">
                <div class="w-full sm:w-1/2">XLS Template runterladen</div>
                <div class="w-full sm:w-1/2">XLS wieder hochladen und SEPA XML erzeugen</div>
            </div>
        </div>
    </div>
@endsection
