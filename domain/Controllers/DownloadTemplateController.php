<?php

namespace XlsToSEPAXml\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use XlsToSEPAXml\Exports\CreditTransferTemplate;
use XlsToSEPAXml\Exports\DirectDebitTemplate;

class DownloadTemplateController
{
    public function getCreditTransferTemplate()
    {
        return Excel::download(new CreditTransferTemplate(), 'sepa-credit-transfer-template.xlsx');
    }

    public function getDirectDebitTemplate()
    {
        return Excel::download(new DirectDebitTemplate(), 'sepa-direct-debit-template.xlsx');
    }
}
